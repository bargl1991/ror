FlatRent::Application.routes.draw do
  resources :photos

  resources :shouts

  resources :offers

  resources :events do
    collection do
         get :likeit
     end
  end

  root :to => "home#create"
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users

  mount Commontator::Engine => '/commontator'
end