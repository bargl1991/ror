class User < ActiveRecord::Base
  mount_uploader :avatar, PictureUploader
  acts_as_commontator
  has_many :events
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

end
